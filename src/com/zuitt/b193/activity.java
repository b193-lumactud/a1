package com.zuitt.b193;
import java.util.Scanner;
public class activity {
    public static void main (String[] args) {

        Scanner myScanner = new Scanner(System.in);

        System.out.println("Input your first name");
        String firstName = myScanner.nextLine().trim();
        System.out.println("First Name: " +firstName);

        System.out.println("Input your last name");
        String lastName = myScanner.nextLine().trim();
        System.out.println("Last Name: " +lastName);

        System.out.println("Enter your first subject grade");
        double firstSubject = myScanner.nextDouble();
        System.out.println("First Subject Grade :" +firstSubject);

        System.out.println("Enter your second subject grade");
        double secondSubject = myScanner.nextDouble();
        System.out.println("Second Subject Grade :" +secondSubject);

        System.out.println("Enter your third subject grade");
        double thirdSubject = myScanner.nextDouble();
        System.out.println("Third Subject Grade :" +thirdSubject);

        double total = firstSubject + secondSubject + thirdSubject;
        double averageScore = total/3;

        System.out.println("Good Day! " +firstName +lastName);
        System.out.println("Your grade average is: " +averageScore);

    }
}
